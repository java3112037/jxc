package com.atguigu.jxc.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * title:
 * author:chk
 * data:
 * description:
 */
@NoArgsConstructor
@Data
public class DamageListGoodsVo {
    @JsonProperty("goodsId")
    private Integer goodsId;
    @JsonProperty("goodsTypeId")
    private Integer goodsTypeId;
    @JsonProperty("goodsCode")
    private String goodsCode;
    @JsonProperty("goodsName")
    private String goodsName;
    @JsonProperty("goodsModel")
    private String goodsModel;
    @JsonProperty("goodsUnit")
    private String goodsUnit;
    @JsonProperty("lastPurchasingPrice")
    private Double lastPurchasingPrice;
    @JsonProperty("price")
    private Double price;
    @JsonProperty("goodsNum")
    private Integer goodsNum;
    @JsonProperty("total")
    private Integer total;
}
