package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;


import java.util.Map;

public interface GoodsService  {


    ServiceVO getCode();


    Map<String,Object> list(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);


    ServiceVO saveOrUpdate(Goods goods);

    ServiceVO delete(Integer goodsId);


    Map<String, Object> getList(Integer page, Integer rows, String nameOrCode);

    Map<String, Object> getHasList(Integer page, Integer rows, String nameOrCode);

    ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);

    ServiceVO deleteStock(Integer goodsId);

    Map<String, Object> getListAlarm();
}
