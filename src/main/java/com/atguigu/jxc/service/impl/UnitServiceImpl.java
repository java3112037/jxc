package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.UnitDao;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.UnitService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * title:
 * author:chk
 * data:
 * description:
 */
@Service
public class UnitServiceImpl implements UnitService {
    @Resource
    private UnitDao unitDao;
    @Override
    public Map<String, Object> getList() {
        Map<String, Object> map = new HashMap<>();
        List<Unit> list= unitDao.getList();
        map.put("rows",list);
        return map;
    }
}
