package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import org.apache.shiro.crypto.hash.Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Resource
    private GoodsDao goodsDao;
    @Resource
    private GoodsTypeDao goodsTypeDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }



    @Override
    public Map<String,Object> list(Integer page, Integer row, String codeOrName, Integer goodsTypeId) {
        Map<String, Object> map = new HashMap<>();
        page = page == 0?1:page;
        int offSet = (page-1)*row;
        List<Goods> goods = goodsDao.getList(offSet,row,codeOrName,goodsTypeId);
        for (Goods good : goods) {
            String name = goodsTypeDao.getGoodsTypeNameById(good.getGoodsTypeId());
            good.setGoodsTypeName(name);
        }
        map.put("total",goodsDao.getGoodsCount(codeOrName,goodsTypeId));
        map.put("rows", goods);
        return map;
    }

    //商品添加或修改
    @Override
    public ServiceVO saveOrUpdate(Goods goods) {
        if(goods.getGoodsId() != null){
            //修改
            goodsDao.updateGoods(goods);
        }else{
            //添加
            Goods goods1 = goodsDao.getGoodsByName(goods.getGoodsName());
            if(goods1 != null){
                return new ServiceVO(ErrorCode.GOODS_ERROR_CODE, ErrorCode.GOODS_ERROR_MESS);
            }
            goodsDao.saveGoods(goods);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    //删除
    @Override
    public ServiceVO delete(Integer goodsId) {
        //1.判断
        Goods goods = goodsDao.getGoodsById(goodsId);
        if(goods.getState() != 0){
            return new ServiceVO(ErrorCode.GOODS_DELETE_ERROR_CODE, ErrorCode.GOODS_DELETE_ERROR_MESS);
        }
        //2.删
        goodsDao.delete(goodsId);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> getList(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map = new HashMap<>();
        page = page == 0?1:page;
        int offset = (page-1)*rows;
        List<Goods> goods = goodsDao.getGoodsByNameOrCode(nameOrCode,offset,rows);
        List<Goods> list = new ArrayList<>();
        for (Goods good : goods) {
            if(good.getInventoryQuantity()<=0){
                String name = goodsTypeDao.getGoodsTypeNameById(good.getGoodsTypeId());
                good.setGoodsTypeName(name);
                list.add(good);
            }
        }
        map.put("total",goodsDao.getCount(nameOrCode));
        map.put("rows",list);
        return map;

    }

    @Override
    public Map<String, Object> getHasList(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map = new HashMap<>();
        page = page == 0?1:page;
        int offset = (page-1)*rows;
        List<Goods> goods = goodsDao.getGoodsByNameOrCode(nameOrCode,offset,rows);
        List<Goods> list = new ArrayList<>();
        for (Goods good : goods) {
            if(good.getInventoryQuantity()>0){
                String name = goodsTypeDao.getGoodsTypeNameById(good.getGoodsTypeId());
                good.setGoodsTypeName(name);
                list.add(good);
            }
        }
        map.put("total",goodsDao.getCount1(nameOrCode));
        map.put("rows",list);
        return map;

    }


    //添加库存、修改数量或成本价
    @Override
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        Goods goods = goodsDao.getGoodsById(goodsId);
        goods.setInventoryQuantity(inventoryQuantity);
        goods.setPurchasingPrice(purchasingPrice);
        goodsDao.updateGoods(goods);

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }


    //删除库存
    @Override
    public ServiceVO deleteStock(Integer goodsId) {
        Goods goods = goodsDao.getGoodsById(goodsId);
        if(goods.getState() == 0){
            goods.setInventoryQuantity(-1);
            goodsDao.updateGoods(goods);
            return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
        }
        return new ServiceVO(ErrorCode.GOODS_KUCUN_ERROR_CODE, ErrorCode.GOODS_KUCUN_ERROR_MESS);
    }


    //查询所有 当前库存量 小于 库存下限的商品信息
    @Override
    public Map<String, Object> getListAlarm() {
        List<Goods> goodsList = goodsDao.getAllGoods();
        List<Goods> list = new ArrayList<>();
        for (Goods goods : goodsList) {
            if(goods.getInventoryQuantity()<goods.getMinNum()){
                String name = goodsTypeDao.getGoodsTypeNameById(goods.getGoodsTypeId());
                goods.setGoodsTypeName(name);
                list.add(goods);
            }
        }
        Map<String, Object> map = new HashMap<>();
        map.put("rows",list);
        return map;
    }
}
