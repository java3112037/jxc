package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.KucunDao;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.KucunService;
import com.atguigu.jxc.util.JSONs;
import com.fasterxml.jackson.core.type.TypeReference;


import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.stream.Collectors;

/**
 * title:
 * author:chk
 * data:
 * description:
 */
@Service
public class KucunServiceImpl implements KucunService {
    @Resource
    private KucunDao kucunDao;


    @Override
    public ServiceVO saveDamageListGoods(DamageList damageList, String damageListGoodsStr,String damageNumber, HttpSession session) {
        User currentUser = (User) session.getAttribute("currentUser");
        damageList.setUserId(currentUser.getUserId());

        kucunDao.save(damageList,damageNumber);

        List<DamageListGoodsVo> damageListGoodsVos = JSONs.strJSONToObj(damageListGoodsStr, new TypeReference<List<DamageListGoodsVo>>() {
        });

        Integer damageListId = damageList.getDamageListId();

        List<DamageListGoods> collect = damageListGoodsVos.stream().map(item -> {
            DamageListGoods damageListGoods = new DamageListGoods();

            damageListGoods.setGoodsId(item.getGoodsId());
            damageListGoods.setGoodsCode(item.getGoodsCode());
            damageListGoods.setGoodsName(item.getGoodsName());
            damageListGoods.setGoodsModel(item.getGoodsModel());
            damageListGoods.setGoodsUnit(item.getGoodsUnit());
            damageListGoods.setGoodsNum(item.getGoodsNum());
            damageListGoods.setPrice(item.getPrice());
            damageListGoods.setTotal(item.getTotal());
            damageListGoods.setDamageListId(damageListId);
            damageListGoods.setGoodsTypeId(item.getGoodsTypeId());
            return damageListGoods;
        }).collect(Collectors.toList());


        kucunDao.saveDamageStr(collect);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO saveOverflowListGoods(OverflowList overflowList, String overflowListGoodsStr, String overflowNumber, HttpSession session) {
        User user = (User) session.getAttribute("currentUser");
        Integer userId = user.getUserId();
        overflowList.setUserId(userId);
        kucunDao.saveOver(overflowList,overflowNumber);

        List<OverflowListGoodsVo> overflowListGoodsVos = JSONs.strJSONToObj(overflowListGoodsStr, new TypeReference<List<OverflowListGoodsVo>>() {
        });
        //需要在sql里开启属性回填
        Integer overflowListId = overflowList.getOverflowListId();
        List<OverflowListGoods> collect = overflowListGoodsVos.stream().map(item -> {
            OverflowListGoods overflowListGoods = new OverflowListGoods();
            overflowListGoods.setGoodsId(item.getGoodsId());
            overflowListGoods.setGoodsCode(item.getGoodsCode());
            overflowListGoods.setGoodsName(item.getGoodsName());
            overflowListGoods.setGoodsModel(item.getGoodsModel());
            overflowListGoods.setGoodsUnit(item.getGoodsUnit());
            overflowListGoods.setGoodsNum(item.getGoodsNum());
            overflowListGoods.setPrice(item.getPrice());
            overflowListGoods.setTotal(item.getTotal());
            overflowListGoods.setOverflowListId(overflowListId);
            overflowListGoods.setGoodsTypeId(item.getGoodsTypeId());
            return overflowListGoods;
        }).collect(Collectors.toList());
        kucunDao.saveOverflowListGoods(collect);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }


    //报损单查询

    @Override
    public  Map<String, Object> getDamageListGoods(String sTime, String eTime,HttpSession session) {
        List<DamageList> list = kucunDao.getDamageListGoods(sTime,eTime);
        User user = (User) session.getAttribute("currentUser");
        Integer userId = user.getUserId();
        String userName = user.getUserName();
        List<DamageList> list1 = new ArrayList<>();
        for (DamageList damageList : list) {
            damageList.setUserId(userId);
            damageList.setTrueName(userName);
            list1.add(damageList);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("rows",list1);
        return map;
    }

    //查询报损单商品信息
    @Override
    public Map<String, Object> getGoodsList(Integer damageListId) {
        List<DamageListGoods> list =  kucunDao.getGoodsList(damageListId);
        Map<String, Object> map = new HashMap<>();
        map.put("rows",list);
        return map;
    }

    //报溢单
    @Override
    public Map<String, Object> getOverflowListGoods(String sTime, String eTime,HttpSession session) {
        List<OverflowList> list = kucunDao.getOverflowListGoods(sTime,eTime);
        User user = (User) session.getAttribute("currentUser");
        Integer userId = user.getUserId();
        String userName = user.getUserName();
        List<OverflowList> list1 = new ArrayList<>();
        for (OverflowList overflowList : list) {
            overflowList.setUserId(userId);
            overflowList.setTrueName(userName);
            list1.add(overflowList);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("rows",list1);
        return map;
    }

    //报溢单商品信息
    @Override
    public Map<String, Object> getOverGoodsList(Integer overflowListId) {
        List<OverflowListGoods> list =  kucunDao.getOverGoodsList(overflowListId);
        Map<String, Object> map = new HashMap<>();
        map.put("rows",list);
        return map;
    }


}
