package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * title:
 * author:chk
 * data:
 * description:
 */
@Service
public class SupplierServiceImpl implements SupplierService {

    @Resource
    private SupplierDao supplierDao;
    @Autowired
    private LogService logService;
    @Override
    public Map<String, Object> getList(Integer page, Integer rows, String supplierName) {
        Map<String, Object> map = new HashMap<>();
        page = page ==0?1:page;
        int offSet = (page-1)*rows;

        List<Supplier> suppliers = supplierDao.getList(offSet,rows,supplierName);

        map.put("total",supplierDao.getSupplierCount(supplierName));
        map.put("rows",suppliers);
        return map;
    }

    @Override
    public ServiceVO saveOrUpdate(Supplier supplier) {
        if(supplier.getSupplierId() != null){
            //修改
            supplierDao.update(supplier);
            logService.save(new Log(Log.UPDATE_ACTION,"修改购物商:"+supplier.getSupplierName()));
        }else{
            //新增
            Supplier supplier1 = supplierDao.findBySupplierName(supplier.getSupplierName());
            if(supplier1 != null){
                return new ServiceVO(ErrorCode.SUPPLIER_ERROR_CODE, ErrorCode.SUPPLIER_ERROR_MESS);
            }else{
                supplierDao.save(supplier);
                logService.save(new Log(Log.INSERT_ACTION,"添加购物商:"+supplier.getSupplierName()));
            }
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    //批量删除
    @Override
    public ServiceVO delete(String ids) {
        String[] split = ids.split(",");
        List<Integer> list = Arrays.stream(split).map(item -> Integer.parseInt(item)).collect(Collectors.toList());
        supplierDao.delete(list);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }
}
