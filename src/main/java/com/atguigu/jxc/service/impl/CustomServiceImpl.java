package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.CustomerService;
import com.atguigu.jxc.service.LogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * title:
 * author:chk
 * data:
 * description:
 */
@Service
public class CustomServiceImpl implements CustomerService {
    @Resource
    private CustomerDao customerDao;
    @Resource
    private LogService logService;
    @Override
    public Map<String, Object> getList(Integer page, Integer rows, String customerName) {
        Map<String, Object> map = new HashMap<>();
        page = page==0?1:page;
        int offset = (page-1)*rows;
        List<Customer> list = customerDao.getList(offset,rows,customerName);

        map.put("total",customerDao.getCount(customerName));
        map.put("rows",list);
        return map;
    }

    @Override
    public ServiceVO saveOrUpdate(Customer customer) {
        if(customer.getCustomerId() != null){
            //修改
            customerDao.update(customer);
            logService.save(new Log(Log.UPDATE_ACTION,"修改客户:"+customer.getCustomerName()));
        }else{
            //添加
            Customer customer1 = customerDao.findByCustomerName(customer.getCustomerName());
            if(customer1 != null){
                return new ServiceVO(ErrorCode.CUSTOMER_ERROR_CODE, ErrorCode.CUSTOMER_ERROR_MESS);
            }else{
                customerDao.save(customer);
                logService.save(new Log(Log.INSERT_ACTION,"添加客户:"+customer.getCustomerName()));
            }
        }

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO delete(String ids) {
        String[] split = ids.split(",");
        List<Integer> list = Arrays.stream(split).map(item -> Integer.parseInt(item)).collect(Collectors.toList());
        customerDao.delete(list);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }
}
