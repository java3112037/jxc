package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;

import java.util.Map;

/**
 * title:
 * author:chk
 * data:
 * description:
 */
public interface CustomerService {
    Map<String, Object> getList(Integer page, Integer rows, String customerName);

    ServiceVO saveOrUpdate(Customer customer);

    ServiceVO delete(String ids);
}
