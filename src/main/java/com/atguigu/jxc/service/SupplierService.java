package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

/**
 * title:
 * author:chk
 * data:
 * description:
 */
public interface SupplierService {
    Map<String, Object> getList(Integer page, Integer rows, String supplierName);

    ServiceVO saveOrUpdate(Supplier supplier);

    ServiceVO delete(String ids);
}
