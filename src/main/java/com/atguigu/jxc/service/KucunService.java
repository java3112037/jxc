package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * title:
 * author:chk
 * data:
 * description:
 */
public interface KucunService {
    ServiceVO saveDamageListGoods(DamageList damageList, String damageListGoodsStr,String damageNumber, HttpSession session);

    ServiceVO saveOverflowListGoods(OverflowList overflowList, String overflowListGoodsStr, String overflowNumber, HttpSession session);

    Map<String, Object> getDamageListGoods(String sTime, String eTime,HttpSession session);

    Map<String, Object> getGoodsList(Integer damageListId);

    Map<String, Object> getOverflowListGoods(String sTime, String eTime,HttpSession session);

    Map<String, Object> getOverGoodsList(Integer overflowListId);
}
