package com.atguigu.jxc.controller;

import com.atguigu.jxc.service.UnitService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * title:
 * author:chk
 * data:
 * description:
 */
@RestController
public class UnitController {

    @Resource
    private UnitService unitService;

    @PostMapping("/unit/list")
    public Map<String,Object> getList(){
        return unitService.getList();
    }
}
