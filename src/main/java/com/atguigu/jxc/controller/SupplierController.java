package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;

import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * title:
 * author:chk
 * data:
 * description:
 */
@RestController
public class SupplierController {
    @Resource
    private SupplierService supplierService;

    //分页查询供应商
    @PostMapping("/supplier/list")
    public Map<String,Object> getSupplierList(Integer page, Integer rows, String supplierName){
        return supplierService.getList(page,rows,supplierName);
    }

    //供应商添加或修改
    @PostMapping("/supplier/save")
    public ServiceVO saveOrUpdate(Supplier supplier){
       return supplierService.saveOrUpdate(supplier);
    }

    //删除
    @PostMapping("/supplier/delete")
    public ServiceVO delete(String ids){

        return supplierService.delete(ids);
    }

}
