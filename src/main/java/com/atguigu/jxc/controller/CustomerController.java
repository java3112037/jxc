package com.atguigu.jxc.controller;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * title:
 * author:chk
 * data:
 * description:
 */
@RestController
public class CustomerController {
    @Resource
    private CustomerService customerService;
    @PostMapping("/customer/list")
    public Map<String,Object> getList(Integer page, Integer rows, String customerName){
        return customerService.getList(page,rows,customerName);
    }

    @PostMapping("/customer/save")
    public ServiceVO saveOrUpdate(Customer customer){
        return customerService.saveOrUpdate(customer);
    }

    @PostMapping("/customer/delete")
    public ServiceVO delete(String ids){
        return customerService.delete(ids);
    }
}
