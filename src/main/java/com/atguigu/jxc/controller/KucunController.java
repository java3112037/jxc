package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.KucunService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * title:
 * author:chk
 * data:
 * description:
 */
@RestController
public class KucunController {
    @Resource
    private KucunService kucunService;
    //商品报损
    @PostMapping("/damageListGoods/save")
    public ServiceVO saveDamageListGoods(DamageList damageList, String damageListGoodsStr, @RequestParam("damageNumber") String damageNumber, HttpSession session){
        return kucunService.saveDamageListGoods(damageList,damageListGoodsStr,damageNumber,session);
    }

    //商品报溢
    @PostMapping("/overflowListGoods/save")
    public ServiceVO saveOverflowListGoods(OverflowList overflowList, String overflowListGoodsStr,
                                           @RequestParam("overflowNumber")String overflowNumber,HttpSession session){
        return kucunService.saveOverflowListGoods(overflowList,overflowListGoodsStr,overflowNumber,session);
    }

    //报损单查询
    @PostMapping("/damageListGoods/list")
    public Map<String,Object> getDamageListGoods(String  sTime, String  eTime,HttpSession session){
        return kucunService.getDamageListGoods(sTime,eTime,session);
    }

    //查询报损单商品信息
    @PostMapping("/damageListGoods/goodsList")
    public Map<String,Object> getGoodsList(Integer damageListId){
        return kucunService.getGoodsList(damageListId);
    }

    //报溢单查询
    @PostMapping("/overflowListGoods/list")
    public Map<String,Object> getOverflowListGoods(String  sTime, String  eTime,HttpSession session){
        return kucunService.getOverflowListGoods(sTime,eTime,session);
    }

    //报溢单商品信息
    @PostMapping("/overflowListGoods/goodsList")
    public Map<String,Object> getOverGoodsList(Integer overflowListId){
        return kucunService.getOverGoodsList(overflowListId);
    }
}
