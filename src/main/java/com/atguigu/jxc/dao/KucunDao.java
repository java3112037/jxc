package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * title:
 * author:chk
 * data:
 * description:
 */
public interface KucunDao {

    void saveDamageStr(List<DamageListGoods> collect);


    void save(@Param("damageList") DamageList damageList,
              @Param("damageNumber")String damageNumber
              );


    void saveOver(@Param("overflowList") OverflowList overflowList, @Param("overflowNumber") String overflowNumber);

    void saveOverflowListGoods(@Param("collect") List<OverflowListGoods> collect);

    List<DamageList> getDamageListGoods(@Param("sTime") String sTime, @Param("eTime") String eTime);

    List<DamageListGoods> getGoodsList(@Param("damageListId") Integer damageListId);

    List<OverflowList> getOverflowListGoods(@Param("sTime") String sTime, @Param("eTime") String eTime);

    List<OverflowListGoods> getOverGoodsList(@Param("overflowListId") Integer overflowListId);
}
