package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @description 商品信息
 */
public interface GoodsDao  {

    String getMaxCode();

    List<Goods> getList(@Param("offSet") int offSet,
                        @Param("row") Integer row,
                        @Param("codeOrName") String codeOrName,
                        @Param("goodsTypeId") Integer goodsTypeId);

    Integer getGoodsCount(@Param("codeOrName") String codeOrName,
                          @Param("goodsTypeId") Integer goodsTypeId);


    void updateGoods(@Param("goods") Goods goods);

    Goods getGoodsByName(@Param("goodsName") String goodsName);

    void saveGoods(Goods goods);

    void delete(@Param("goodsId") Integer goodsId);

    Goods getGoodsById(@Param("goodsId") Integer goodsId);

    List<Goods> getGoodsByNameOrCode(@Param("nameOrCode") String nameOrCode,@Param("offset") int offset, @Param("rows") Integer rows);

    Integer getCount(@Param("nameOrCode") String nameOrCode);

    Integer getCount1(@Param("nameOrCode") String nameOrCode);

    List<Goods> getAllGoods();
}
