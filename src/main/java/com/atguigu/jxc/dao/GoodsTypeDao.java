package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);


    String getGoodsTypeNameById(@Param("goodsTypeId") Integer goodsTypeId);

    GoodsType getGoodsTypeByName(@Param("goodsTypeName") String goodsTypeName);

    void saveType(@Param("goodsTypeName") String goodsTypeName, @Param("pId") Integer pId);

    void delete(@Param("goodsTypeId") Integer goodsTypeId);
}
