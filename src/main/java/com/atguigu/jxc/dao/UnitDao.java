package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Unit;

import java.util.List;

/**
 * title:
 * author:chk
 * data:
 * description:
 */
public interface UnitDao {

    List<Unit> getList();
}
