package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * title:
 * author:chk
 * data:
 * description:
 */
public interface CustomerDao {
    List<Customer> getList(@Param("offset") int offset, @Param("rows") Integer rows, @Param("customerName") String customerName);

    Integer getCount(@Param("customerName") String customerName);

    void update(Customer customer);

    Customer findByCustomerName(String customerName);

    void save(Customer customer);

    void delete(List<Integer> list);
}
