package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * title:
 * author:chk
 * data:
 * description:
 */
public interface SupplierDao {

    List<Supplier> getList(@Param("offSet") int offSet, @Param("rows") Integer rows, @Param("supplierName") String supplierName);

    Integer getSupplierCount(@Param("supplierName") String supplierName);

    void update( Supplier supplier);

    Supplier findBySupplierName(@Param("supplierName") String supplierName);

    void save( Supplier supplier);

    void delete(List<Integer> list);
}
