package com.atguigu.jxc.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

@Slf4j
public class JSONs {
    //单例且线程安全的 所以声明在外面
    private static final ObjectMapper objectMapper = new ObjectMapper();
    
    public static String toJSONStr(Object obj){

        String s = null;
        try {
            s = objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            log.error("json转换错误");
            throw new RuntimeException(e);
        }

        return s;
    }

    public static<T> T strJSONToObj(String json,Class<T> valueType) {
        T t = null;
        try {
            if(!StringUtils.hasText(json)) return null;
            t = objectMapper.readValue(json, valueType);
        } catch (JsonProcessingException e) {
            log.error("json转换失败");
            throw new RuntimeException(e);
        }
        return t;
    }

    public static<T> T strJSONToObj(String json, TypeReference<T> valueType) {
        T t = null;

            try {
                t = objectMapper.readValue(json, valueType);
            } catch (JsonProcessingException e) {
                log.error("json转换失败");
                throw new RuntimeException(e);
            }
            return t;
        }


}
